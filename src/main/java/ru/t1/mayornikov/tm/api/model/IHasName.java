package ru.t1.mayornikov.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
