package ru.t1.mayornikov.tm.api.controller;

public interface ICommandController {

    void initDemoData();

    void showSystemInfo();

    void showVersion();

    void showAbout();

    void showHelp();

}
